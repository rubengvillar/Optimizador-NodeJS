'use strict';

const ContactModel = require("../models/contact-model"),
errors = require("../middlewares/errors"),
    cm = new ContactModel();

class ContactController {
	send(req, res, next) {
        let contact = {
            name: req.body.name,
            email: req.body.email,
            affair: req.body.affair,
            message: req.body.message
        };
        cm.send(contact, function (error){
                if (!error) {
                    res.render('contact', {
                        title: 'Contacto',
                        message: 'Mensaje enviado correctamente'
                    });
                } else {
                    return errors.http500;
                }
        });
		
	}
}

module.exports = ContactController;