'use strict';

const LinearModel = require("../models/linear-model"),
errors = require("../middlewares/errors"),
    lm = new LinearModel();

class LinearController {
	index(req, res, next) {
		res.render('linear', {
            title: 'Optimizador lineal',
            newsmessage: 'Versión alpha 0.0.1 del optimizador lineal'
        });
	}
    getLinear(req, res, next){
        let profilesTypology = req.body.typology,
         profilesMeasure = req.body.measure;
        let profiles = {
            barMeasure: Number(req.body.bar),
            barWaste: Number(req.body.waste),
            currentProfiles: [[]],
            cutBar: []
		};
        for(let i = 0; i < profilesTypology.length; i++){
            profiles.currentProfiles[i] = {
                typology: profilesTypology[i],
                measure: Number(profilesMeasure[i]),
            };
        }
        lm.getOptimizerLinear(profiles, (date)=>{
            if(date){
                res.render('linear', {
                    title: 'Optimizador lineal',
                    optimizedProfiles: date,
                });
            }
        });

    }
}

module.exports = LinearController;