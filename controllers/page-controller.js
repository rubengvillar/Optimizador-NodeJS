'use strict';

const errors = require("../middlewares/errors");

class PageController {
	index(req, res, next) {
		res.render('index', {
			title: 'Inicio'
		});
	}
  apps(req, res, next) {
		res.render('apps', {
			title: 'Aplicaciones'
		});
	}
  about(req, res, next) {
		res.render('about', {
			title: 'Acerca'
		});
	}
  contact(req, res, next) {
		res.render('contact', {
			title: 'Contacto'
		});
	}
}

module.exports = PageController;