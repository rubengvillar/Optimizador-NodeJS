'use strict';

const ContactController = require('../controllers/contact-controller'),
    express = require('express'),
    router = express.Router(),
    cc = new ContactController();

router
    .post('/contact', cc.send)

module.exports = router;