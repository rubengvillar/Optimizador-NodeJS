'use strict';

const LinearController = require('../controllers/linear-controller'),
    express = require('express'),
    router = express.Router(),
    lc = new LinearController();

router
    .get('/linear', lc.index)
    .post('/linear', lc.getLinear)

module.exports = router;