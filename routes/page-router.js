'use strict';

const PageController = require('../controllers/page-controller'),
    express = require('express'),
    router = express.Router(),
    pc = new PageController();

router
    .get('/', pc.index)
    .get('/apps', pc.apps)
    .get('/about', pc.about)
    .get('/contact', pc.contact)

module.exports = router;