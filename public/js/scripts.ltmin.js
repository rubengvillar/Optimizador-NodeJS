(function (i, s, o, g, r, a, m) {
    i['GoogleAnalyticsObject'] = r;
    i[r] = i[r] || function () {
        (i[r].q = i[r].q || []).push(arguments);
    }, i[r].l = 1 * new Date();
    a = s.createElement(o),
        m = s.getElementsByTagName(o)[0];
    a.async = 1;
    a.src = g;
    m.parentNode.insertBefore(a, m);
})(window, document, 'script', 'https://www.google-analytics.com/analytics.js', 'ga');
ga('create', 'UA-76354965-1', 'auto');
ga('send', 'pageview');
(function (w, d) {
    var Select = (function () {
        function Select() {
        }
        Select.prototype.qs = function (selector) {
            return d.querySelector(selector);
        };
        Select.prototype.qsa = function (selector) {
            return d.querySelectorAll(selector);
        };
        Select.prototype.id = function (selector) {
            return d.getElementById(selector);
        };
        Select.prototype.class = function (selector) {
            return d.getElementsByClassName(selector);
        };
        Select.prototype.tag = function (selector) {
            return d.getElementsByTagName(selector);
        };
        return Select;
    }());
    var select = new Select(), toggleMenu = select.qs('#toggle-main-menu'), menu = select.qs('#nav-menu');
    toggleMenu.addEventListener('click', function () {
        menu.classList.toggle('active');
    });
    if (select.id('optimizer-agregar')) {
        var profileAgregar = select.id('optimizer-agregar');
        profileAgregar.addEventListener('click', function () {
            var profiles = select.id('all-profiles');
            var newProfile = d.createElement('div');
            newProfile.className = 'optimizer-form-profile';
            newProfile.innerHTML = "<input type=\"text\" placeholder=\"Tipologia\" name=\"typology\" class=\"optimizer-form-profile-camp\" required>\n            <input type=\"number\" placeholder=\"Medida\" name=\"measure\" class=\"optimizer-form-profile-camp\" required>";
            profiles.appendChild(newProfile);
        });
    }
})(window, document);
function initMap() {
    var myLatLng = { lat: -37.9740214, lng: -57.5845951 };
    var map = new google.maps.Map(document.getElementById('map'), {
        center: myLatLng,
        scrollwheel: true,
        zoom: 18
    });
    var marker = new google.maps.Marker({
        map: map,
        position: myLatLng,
        title: 'Lastwer.com'
    });
}
