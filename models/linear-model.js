'use strict'

class LinearModel{
    getOptimizerLinear(date, cb){
    // Funciones de comparacion

    function MayorAMenor(num1, num2) {
        return num2.measure - num1.measure;
    }
    
    //Función de Ordenamiento.
    date.currentProfiles.sort(MayorAMenor);

    // Optimizacion de la barra.
    let j = 0,
     currentProfiles = date.currentProfiles, 
     bars = [0];
    while (currentProfiles.length){
        if(bars[j] + currentProfiles[0].measure <= date.barMeasure - date.barWaste){
            bars[j] += currentProfiles[0].measure;
            if(date.cutBar[j] == undefined){
                date.cutBar[j] = [];
            }
            date.cutBar[j].push(currentProfiles.shift());
            j = 0;
        } else {
            j++;
            bars[j] = bars[j] || 0;
        }
    }
    cb(date);
    //fin optimizador
    }
}

module.exports = LinearModel;