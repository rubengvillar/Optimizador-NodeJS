'use strict'
const nodemailer = require('nodemailer');
const contactConf = require('./contact-conf');
class ContactModel{
    send(contact, cb){
        // create reusable transporter object using the default SMTP transport
        let transporter = nodemailer.createTransport(contactConf);
        // setup email data with unicode symbols
        let mailOptions = {
            from: `${contact.name} <${contact.email}>`, // sender address
            to: 'contacto@lastwer.com', // list of receivers
            subject: contact.affair, // Subject line
            text: contact.message, // plain text body
            html: `${contact.message}` // html body
        };

        // send mail with defined transport object
        transporter.sendMail(mailOptions, (error, info) => {
            if (error) {
                cb(error);
            } else {
                console.log('Message %s sent: %s', info.messageId, info.response);
                cb();
            }            
        });
    }
}

module.exports = ContactModel;